#Zabbix Server Playbook
  To run the playbook, create a file named host and follow the pattern at hosts.example, adding your server(s) ip(s).

  (For more information about the variables used, see below)

  Then, from the command line, run:
  ```
  ansible-playbook -i hosts site.yaml
  ```

##roles:
###zabbix-server:
Install and configure Zabbix Server, Zabbix Agent and Zabbix Web Interface with MySQL database.

###slack_integration:
Copy the scripts for adding Slack integration script.

To run this role you need to set those host variables in your hosts file (read vars section for more info):

* slack_integration
* webhook_url

Also, you need to add the media type manually at Zabbix Web Interface, because the role only imports the script. Follow the steps at https://rivendel.atlassian.net/wiki/spaces/RIVENDEL/pages/44081505/Integra+o+zabbix+com+slack.

###remove_aws_innactive_servers:
Remove AWS machines that are present on Zabbix Server but doesn't exists anymore. This way, false alarms are avoided.

You have to add this item manually at Zabbix Web Interface, the role only imports the script.

To run this role, you need to define those variables:

* zabbix_user

* zabbix_pass

* zabbix_url

###pager_duty_integration
Installs software for pager duty integration.

To run this role you need to set those host variables in your host file(read vars section below for more info):

* pager_duty_integration
* integration_key

Also, you need to create the media type manually at Zabbix Web Interface. Follow the steps at https://rivendel.atlassian.net/wiki/spaces/TREINA/pages/6815958/Integra+o+zabbix+pagerduty.

##vars:
* **_zabbix_user:_** zabbix admin user (*default*: Admin)

* **_zabbix_pass:_** zabbix admin password (*default*: zabbix)

* **_zabbix_url:_**  zabbix server dns or ip address (*default*: http://localhost/zabbix)


* **_slack_integration:_** set it to "true" to add slack integration (*default*: false)

* **_webhook_url:_** webhook url for slack integration


* **_pager_duty_integration:_** set it to "true" to add pager duty integration (*default*: false)

* **_integration_key:_** integration key for pager duty


* **_ansible_python_interpreter:_** path to python binary to run ansible commands on host (*default*: /usr/bin/python27)
