#How to run
  To run the playbook, create a *hosts* file and follow the pattern at hosts.example, adding your server(s) ip(s) and variables.

  To quickstart an inventory, run the python script that uses the AWS CLI to find the active hosts and save the output into a file.

  (For more information about the variables used and the script, see below)

  To simply run the playbook, use:

  ```
  ansible-playbook -i hosts site.yaml
  ```

  To run without asking for ssh connection confirmation:

  ```
  ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts site.yaml
  ```

  **Remember to change _hosts_ to your inventory file path.**

  The script will generate an Ansible inventory to run the *zabbix-agent* role.

  Run this script and role from a machine with Zabbix Server and AWS CLI **installed** and **configured**.

#Roles:
##zabbix-agent:
  Install and configure Zabbix Agent. This role has been tested in Ubuntu 16.04 and Amazon Linux

  Define the variables listed below in your *hosts* file and/or inside the group_vars files before running this role.

  The configuration file is only imported to the host when Zabbix Agent is installed for the first time. That means if the host already have Zabbix Agent installed, the configuration file isn't overwritten. To change this behavior, remove the line ```  when: zabbix_agent_already_installed.changed``` from the file *roles/zabbix-agent/tasks/main.yaml*.

#Vars:
* **_remote_user:_** The username to login to the machine

* **_ansible_ssh_private_key_file:_** The ssh secret key path to login to the machine

* **_zabbix_server_address:_** Address of the Zabbix Server to send data

* **_agent_hostname:_** Agent hostname to be viewed on the Zabbix Server

* **_zabbix_version:_** Zabbix Agent major and minor version to be installed  (*default*: 3.4)

* **_zabbix_patch:_** Zabbix Agent patch release version to be installed  (*default*: 8)

* **host_meta_data:_** Is used at host auto-registration process (*default*: os-linux)

* **RedHat_version:_** Used to download the right repo for Zabbix Agent (*default*: 6)

##Where to define the variables:
  You can define the variables in any of the group_vars files (all, Debian or RedHat), or create
  another group_vars file (and remember to import it at *site.yaml*) or define directly in the inventory file (hosts file).

  More info: http://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html

  * Don't forget to add the Zabbix Server address in *group_vars/all*
  * Check the repo URLs at *group_vars/Debian* and *group_vars/RedHat*   
  * Check the Zabbix Version to install at *group_vars/all*

##Python Script
  The script in python creates a default hosts file output, but it doesn't create a file automatically, use ```create_inventory.py > hosts``` to save the output from the script to a file named hosts. You may have to edit the hosts file to configure some variables, remove hosts that you don't want to run the ansible role, etc.
