#!/usr/bin/env python3
import boto3

def get_aws_ec2_infos():
    instances = []

    filters = [{'Name': 'instance-state-name', 'Values': ['running']}]
    aws_client = boto3.client('ec2')

    response = aws_client.describe_instances(Filters=filters)

    untitledId = 0
    for reservation in response['Reservations']:
        for instance in reservation['Instances']:

            items = {
                # 'id': instance['InstanceId'],
                'private_ip': instance['PrivateIpAddress'],
                'key_name':   instance['KeyName']
            }

            try:
                hasAName = False
                for tag in instance['Tags']:
                    if tag['Key'] == 'Name':
                        items['name'] = tag['Value']
                        hasAName      = True

                    if tag['Key'] == 'Nome':
                        items['name'] = tag['Value']
                        hasAName      = True

                if(hasAName == False):
                    raise KeyError('The instance does\'nt have a tag name')

            except KeyError:
                items['name'] = instance['PrivateIpAddress']

            instances.append(items)

    return instances

instances = get_aws_ec2_infos()

for instance in instances:
    print(instance['private_ip'] + ' remote_user=ec2-user' + ' ansible_ssh_private_key_file=' + instance['key_name'] + '.pem' + ' agent_hostname="' + instance['name'] + '"')
